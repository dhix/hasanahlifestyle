var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var cors = require("cors");
var log = require('./util/logger.js').LOG;

app.use(express.static('./public/images/'));
app.use(cors());

app.use(bodyParser.json({ limit: '5mb' }));
app.use(bodyParser.urlencoded({ extended: true, limit: '5mb' }));

var scheduleJob = require("./util/scheduleJob.js");
var routes = require("./routes/routes.js")(app);

app.listen(8180);
log.info("Server started");