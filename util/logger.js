var log4js = require('log4js');

log4js.configure({
    appenders: [
        { type: 'console' },
        { type: 'file', filename: '/home/logs/hasanah.log', category: 'hasanah' }
    ]
});
var logger = log4js.getLogger('hasanah');
logger.setLevel('DEBUG');
Object.defineProperty(exports, "LOG", {
            value:logger
});