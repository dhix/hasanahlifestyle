var schedule = require('node-schedule');
var log = require('./logger.js').LOG;
 
var j = schedule.scheduleJob('4 * * * * *', function() {
    try {
        var token = require("../models/token");
        token("token");
        token.deleteAll(function(err, result) {
//            if (err) {
//                log.info("Delete token error : " + err);
//            } else {
//                log.info("Delete token success : " + result);
//            }
        });
    } catch (err) {
        log.error("scheduleJob (TOKEN) : " + err);
    }
    
    try {
        var umroh = require("../models/umroh");
        umroh("umroh");
        umroh.disableOldUmroh(function(err, result) {
//            if (err) {
//                log.info("Disable umroh error : " + err);
//            } else {
//                log.info("Disable umroh success : " + result);
//            }
        });
    } catch (err) {
        log.error("scheduleJob (UMROH) : " + err);
    }
});