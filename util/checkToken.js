require('x-date');

function check() {
};

check.token = function(req, callback) {
    try {
        var token = require("../models/token");
        token("token");
        token.getById(req, function(err, result) {
            if (err) {
                callback(false);
            } else {
                var now = new Date().format('yyyy-mm-dd HH:MM:ss');
                
                if (result.exp > now) {
                    callback(true);
                } else {
                    callback(false);
                }
            }
        });
    } catch (err) {
        callback(false);
    }
};

module.exports = check;