var nodemailer = require('nodemailer');

function mail() {
    // create reusable transporter object using the default SMTP transport
};

mail.send = function(docType, callback) {
    try {
        var transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: 'yudhix.kurniawan@gmail.com',
                pass: 'dhix>>>>'
            }
        });
    
        var model = require("../models/" + docType);
        model(docType);
        model.getAll(null, function(err, result) {
            if (!err) {
                var mailOptions = {
                    from: 'Hasanah Lifestyle BNI Syariah', // sender address
                    to: result.email, // list of receivers
                    subject: result.title, // Subject line
                    text: result.content // plain text body
                    //html: content // html body
                };

                // send mail with defined transport object
                transporter.sendMail(mailOptions, function(error, info) {
                    if (error) {
                        return console.log(error);
                    }
                    console.log('Message %s sent: %s', info.messageId, info.response);
                    callback(true);
                });
            }
        });
    } catch(err) {
        console.log(err);
        callback(false);
    }
};

module.exports = mail;