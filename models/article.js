var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
var fs = require('fs');
require('x-date');
var appConfig = require('../config/appConfig');
var dbConfig = require('../config/dbConfig');
var db = dbConfig.getDb();
var queryFactory = dbConfig.getQueryFactory();
var documentType = "";


function model(docType) {
    documentType = docType;
};

model.save = function(req, callback) {
    log.info("modelArticle.save : " + req.body);
    
    var documentID = req.body.id;
    var id_article_sub_category = req.body.id_article_sub_category;
    var title = req.body.title;
    
    if (!id_article_sub_category || !title) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        var jsonData = {
            type: documentType,
            id_article_sub_category: id_article_sub_category,
            title: title,
            description: req.body.description,
            content: req.body.content,
            headline: req.body.headline,
            img: req.body.img,
            author: req.body.author,
            time: req.body.time,
            status: req.body.status
        };
        
        var add = false;

        if (!documentID) {
            add = true;
            documentID = new Date().format('yyyymmddHHMMss') + uuid.v4();
        }
    
        db.upsert(documentID, jsonData, function(error, result) {
            if (error) {
                log.error("modelArticle.save : " + error);
                callback(error, null);
            } else {
                if (add) {
                    callback(null, {status: "success", message: "Data telah disimpan.", id: documentID});
                } else {
                    callback(null, {status: "success", message: "Data telah diubah.", id: documentID});
                }
            }
        });
    }
};

model.delete = function(req, callback) {
    log.info("modelArticle.delete : " + req.body);
    
    var id = req.body.id;
    
    if (!id) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        db.remove(id, function (err, res) {
            if (err) {
                log.error("modelArticle.delete : " + err);
                callback(err, null);
            } else {
                callback(null, {status: "success", message: "Data telah dihapus."});
            }
        });
    }
};

model.getById = function(req, callback) {
    log.info("modelArticle.getById : " + req.body);
    
    var id = req.body.id;
    
    if (!id) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        var statement = "SELECT META(doc).id, doc.id_article_sub_category, " 
                + "articlesubcategory.name AS name_article_sub_category, "
                + "doc.headline, doc.img, doc.title, doc.time, doc.author, "
                + "doc.description, doc.content, doc.status "
                + "FROM " + dbConfig.dbName + " doc "
                + "JOIN " + dbConfig.dbName + " articlesubcategory "
                + "ON KEYS doc.id_article_sub_category "
                + "WHERE META(doc).id='" + id + "' LIMIT 1";
        var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
        var dbReq = db.query(query);
        dbReq.on('row', function(row) {
            callback(null, row);
        });
        dbReq.on('error', function(err) {
            log.error("modelArticle.getByid : " + err);
            callback(err, null);
        });
    }
};

model.getAll = function(req, callback) {
    var status = req.body.status;
    var limit = req.body.limit;
    var page = req.body.page;
    var offset = 0;
    var filter = "";
    
    if (!limit) {
        limit = 10;
    }
    if (page && page>0) {
        offset = (page - 1) * limit;
    }
    if (status) {
        filter = " AND doc.status=" + status;
    }
    
    var statement = "SELECT META(doc).id, doc.id_article_sub_category, " 
            + "articlesubcategory.name AS name_article_sub_category, "
            + "doc.headline, doc.img, doc.title, doc.time, doc.author, "
            + "doc.description, doc.content, doc.status "
            + "FROM " + dbConfig.dbName + " doc "
            + "JOIN " + dbConfig.dbName + " articlesubcategory "
            + "ON KEYS doc.id_article_sub_category "
            + "WHERE doc.type='" + documentType + "'" + filter + " ORDER BY doc.id " 
            + "LIMIT " + limit + " OFFSET " + offset;

    var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
    db.query(query, function(err, result) {
        if (err) {
            log.error("modelArticle.getAll : " + err);
            callback(err, null);
        } else {
            callback(null, result);
        }
    });
};

model.getCount = function(req, callback) {
    var key = req.body.key;
    var status = req.body.status;
    var filter = "";
    
    if (status) {
        filter = " AND doc.status=" + status;
    }
    if (key) {
        filter = " AND (doc.title LIKE '%" + key + "%' OR doc.description LIKE '%" + key + "%')" + filter;
    }
    
    var statement = "SELECT COUNT(META(doc).id) AS total "
            + "FROM " + dbConfig.dbName + " doc "
            + "WHERE doc.type='" + documentType + "'" + filter;

    var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
    db.query(query, function(err, result) {
        if (err) {
            log.error("modelArticle.getCount : " + err);
            callback(err, null);
        } else {
            callback(null, result);
        }
    });
};

model.getByIdArticleSubCategory = function(req, callback) {
    log.info("modelArticle.getByIdArticleSubCategory : " + req.body);
    
    var id_article_sub_category = req.body.id_article_sub_category;
    var status = req.body.status;
    var filter = "";
    
    if (status) {
        filter = " AND doc.status=" + status;
    }
    
    var statement = "SELECT META(doc).id, doc.id_article_sub_category, " 
            + "articlesubcategory.name AS name_article_sub_category, "
            + "doc.headline, doc.img, doc.title, doc.time, doc.author, "
            + "doc.description, doc.content, doc.status "
            + "FROM " + dbConfig.dbName + " doc "
            + "JOIN " + dbConfig.dbName + " articlesubcategory "
            + "ON KEYS doc.id_article_sub_category "
            + "WHERE doc.id_article_sub_category='" + id_article_sub_category + "' "
            + "AND doc.type='" + documentType + "'" + filter + " ORDER BY doc.id";
    var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
    db.query(query, function(err, result) {
        if (err) {
            log.error("modelArticle.save : " + err);
            callback(err, null);
        } else {
            callback(null, result);
        }
    });
};

model.getHeadline = function(req, callback) {
    log.info("modelArticle.getHeadline : " + req.body);
    
    var status = req.body.status;
    var filter = "";
    
    if (status) {
        filter = " AND doc.status=" + status;
    }
    
    var statement = "SELECT META(doc).id, doc.id_article_sub_category, " 
            + "articlesubcategory.name AS name_article_sub_category, "
            + "doc.headline, doc.img, doc.title, doc.time, doc.author, "
            + "doc.description, doc.content, doc.status "
            + "FROM " + dbConfig.dbName + " doc "
            + "JOIN " + dbConfig.dbName + " articlesubcategory "
            + "ON KEYS doc.id_article_sub_category "
            + "WHERE doc.headline=true AND doc.status=1 "
            + "AND doc.type='" + documentType + "'" + filter + " ORDER BY doc.id";
    var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
    db.query(query, function(err, result) {
        if (err) {
            log.error("modelArticle.save : " + err);
            callback(err, null);
        } else {
            callback(null, result);
        }
    });
};

model.search = function(req, callback) {
    var key = req.body.key;
    var status = req.body.status;
    var limit = req.body.limit;
    var page = req.body.page;
    var offset = 0;
    var filter = "";
    
    if (!limit) {
        limit = 10;
    }
    if (page && page>0) {
        offset = (page - 1) * limit;
    }
    if (status) {
        filter = " AND status=" + status;
    }
    
    var statement = "SELECT META(doc).id, doc.id_article_sub_category, " 
            + "articlesubcategory.name AS name_article_sub_category, "
            + "doc.headline, doc.img, doc.title, doc.time, doc.author, "
            + "doc.description, doc.content, doc.status "
            + "FROM " + dbConfig.dbName + " doc "
            + "JOIN " + dbConfig.dbName + " articlesubcategory "
            + "ON KEYS doc.id_article_sub_category "
            + "WHERE (doc.title LIKE '%" + key + "%' OR doc.description LIKE '%" + key + "%') " 
            + "AND doc.type='" + documentType 
            + "'" + filter + " ORDER BY doc.id " 
            + "LIMIT " + limit + " OFFSET " + offset;
    var query = queryFactory.fromString(statement);
    db.query(query, function(err, result) {
        if (err) {
            log.error("modelArticle.save : " + err);
            callback(err, null);
        } else {
            callback(null, result);
        }
    });
};

module.exports = model;