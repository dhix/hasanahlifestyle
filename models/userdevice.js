var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
require('x-date');
var dbConfig = require('../config/dbConfig');
var db = dbConfig.getDb();
var queryFactory = dbConfig.getQueryFactory();
var documentType = "";


function model(docType) {
    documentType = docType;
};

model.save = function(req, callback) {
    var documentID = req.body.id;
    var email = req.body.email;
    var phone = req.body.phone;
    var username = req.body.username;
    var pass = req.body.pass;
    
    if (!email || !phone || !username || !pass) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
        return;
    }
    var jsonData = {
        type: documentType,
        uuid: req.body.uuid,
        device: req.body.device,
        name: req.body.name,
        birthday: req.body.birthday,
        phone: phone,
        address: req.body.address,
        email: email,
        username: username, 
        pass: pass
    };

    var add = false;
    var onRow = false;

    if (!documentID) {
        add = true;
        documentID = new Date().format('yyyymmddHHMMss') + uuid.v4();
    }

    var statement = "SELECT META(doc).id, uuid, device, name, birthday, phone, address, email "
            + "FROM " + dbConfig.dbName + " AS doc "
            + "WHERE doc.email='" + email + "' OR phone='" + phone + "' AND type='" + documentType + "' LIMIT 1";

    var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
    var dbReq = db.query(query);
    dbReq.on('row', function(row) {
        onRow = true;
        if (add) {
            callback({status: "success", message: "Email atau No Hp telah terdaftar."}, null);
        } else if (row.id !== documentID) {
            callback({status: "success", message: "Email atau No Hp telah terdaftar."}, null);
        } else {
            db.upsert(documentID, jsonData, function(error, result) {
                if (error) {
                    callback(error, null);
                } else {
                    if (add) {
                        callback(null, {status: "success", message: "Data telah disimpan.", id: documentID});
                    } else {
                        callback(null, {status: "success", message: "Data telah diubah.", id: documentID});
                    }
                }
            });
        }
    });
    dbReq.on('end', function(row) {
        if (!onRow) {
            db.upsert(documentID, jsonData, function(error, result) {
                if (error) {
                    callback(error, null);
                } else {
                    callback(null, {status: "success", message: "Data telah disimpan."});
                }
            });
        }
    });
    dbReq.on('error', function(err) {
        db.upsert(documentID, jsonData, function(error, result) {
            if (error) {
                callback(error, null);
            } else {
                if (add) {
                    callback(null, {status: "success", message: "Data telah disimpan.", id: documentID});
                } else {
                    callback(null, {status: "success", message: "Data telah diubah.", id: documentID});
                }
            }
        });
    });
};

model.delete = function(req, callback) {
    var id = req.body.id;
    
    if (!id) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        db.remove(id, function (err, res) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, {status: "success", message: "Data telah dihapus."});
            }
        });
    }
};

model.getByUuid = function(req, callback) {
    var uuid = req.body.uuid;
    
    log.info("modelUserdevice.getByUuid : uuid - " + uuid);
    
    if (!uuid) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        var statement = "SELECT META(doc).id, uuid, device, name, birthday, phone, address, email "
                + "FROM " + dbConfig.dbName + " AS doc " 
                + "WHERE uuid = '" + uuid + "' AND doc.type='" + documentType + "'";
        var query = queryFactory.fromString(statement);
        db.query(query, function(err, result) {
            if(err) {
                callback(err, null);
            } else {
                callback(null, result);
            }
        });
    }
};

model.getById = function(req, callback) {
    var id = req.body.id;
    
    if (!id) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        db.get(id, function(err, result) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, result.value);
            }
        });
    }
};

model.getAll = function(req, callback) {
    var limit = req.body.limit;
    var page = req.body.page;
    var offset = 0;
    
    if (!limit) {
        limit = 10;
    }
    if (page && page>0) {
        offset = (page - 1) * limit;
    }
    
    var statement = "SELECT META(doc).id, uuid, device, name, birthday, phone, address, email "
            + "FROM " + dbConfig.dbName + " AS doc "
            + "WHERE type='" + documentType + "' ORDER BY doc.id LIMIT " + limit + " OFFSET " + offset;
    var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
    db.query(query, function(err, result) {
        if (err) {
            callback(err, null);
        } else {
            callback(null, result);
        }
    });
};

model.getCount = function(req, callback) {
    var key = req.body.key;
    var filter = "";
    
    if (key) {
        key = key.toLowerCase();
        filter = filter + " AND LOWER(name) LIKE '%" + key + "%'";
    }
    
    var statement = "SELECT COUNT(META(doc).id) AS total "
            + "FROM " + dbConfig.dbName + " doc "
            + "WHERE doc.type='" + documentType + "'" + filter;

    var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
    db.query(query, function(err, result) {
        if (err) {
            log.error("modelArticle.getCount : " + err);
            callback(err, null);
        } else {
            callback(null, result);
        }
    });
};

model.getReport = function(req, callback) {
    var statement = "SELECT META(doc).id, uuid, device, name, birthday, phone, address, email "
            + "FROM " + dbConfig.dbName + " AS doc "
            + "WHERE type='" + documentType + "' ORDER BY doc.id";
    var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
    db.query(query, function(err, result) {
        if (err) {
            callback(err, null);
        } else {
            callback(null, result);
        }
    });
};

model.login = function(req, callback) {
    var username = req.body.username;
    var pass = req.body.pass;
    
    if (!username || !pass) {
        callback({status: "error", message: "Username dan password tidak valid."}, null);
        return;
    }
    
    var statement = "SELECT META(doc).id, uuid, device, name, birthday, phone, address, email "
            + "FROM " + dbConfig.dbName + " AS doc "
            + "WHERE type='" + documentType + "' " 
            + "AND username='" + username + "' "
            + "AND pass='" + pass + "' ORDER BY doc.id";
    console.log("statement=" + statement);
    var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
    db.query(query, function(err, result) {
        if (err) {
            callback(err, null);
        } else if (result.length===0) {
            callback({status: "error", message: "Username dan password tidak valid."}, null);
        } else {
            callback(null, result[0]);
        }
    });
};

model.search = function(req, callback) {
    var key = req.body.key;
    var limit = req.body.limit;
    var page = req.body.page;
    var offset = 0;
    
    if (!limit) {
        limit = 10;
    }
    if (page && page>0) {
        offset = (page - 1) * limit;
    }
    
    if (!key) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        var statement = "SELECT META(doc).id, uuid, device, name, birthday, phone, address, email "
                + "FROM " + dbConfig.dbName + " AS doc " 
                + "WHERE name LIKE '%$1%' AND doc.type='" + documentType + "' LIMIT " + limit + " OFFSET " + offset;
        var query = queryFactory.fromString(statement);
        db.query(query, [key], function(err, result) {
            if(err) {
                callback(err, null);
            } else {
                callback(null, result);
            }
        });
    }
};

module.exports = model;