var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
require('x-date');
//var fs = require('fs');
//var appConfig = require('../config/appConfig');
var dbConfig = require('../config/dbConfig');
var db = dbConfig.getDb();
var queryFactory = dbConfig.getQueryFactory();
var documentType = "";


function model(docType) {
    documentType = docType;
};

model.save = function(req, callback) {
    var documentID = req.body.id;
    
    var jsonData = {
        type: documentType,
        name: req.body.name,
        img: req.body.img,
        city: req.body.city,
        departure: new Date(req.body.departure),
        price: req.body.price,
        packet: req.body.packet,
        plane: req.body.plane,
        hotel: req.body.hotel,
        phone: req.body.phone,
        quota: req.body.quota,
        content: req.body.content,
        status: req.body.status
    };

    //log.info("modelUmroh.save : " + JSON.stringify(jsonData));
    
    var add = false;

    if (!documentID) {
        add = true;
        documentID = new Date().format('yyyymmddHHMMss') + uuid.v4();
    }

    db.upsert(documentID, jsonData, function(error, result) {
        if (error) {
            callback(error, null);
        } else {
            if (add) {
                callback(null, {status: "success", message: "Data telah disimpan.", id: documentID});
            } else {
                callback(null, {status: "success", message: "Data telah diubah.", id: documentID});
            }
        }
    });
};

model.delete = function(req, callback) {
    var id = req.body.id;
    
    log.info("modelUmroh.delete : " + id);
    
    if (!id) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        db.remove(id, function (err, res) {
            if (err) {
                callback(err, null);
            } else {
//                img = img.replace(appConfig.SERVER_ADDRESS, "");
//                fs.exists('./public/images/' + img, function(exists) {
//                    if (exists) {
//                        fs.unlink('./public/images/' + img);
//                    }
//                });
                callback(null, {status: "success", message: "Data telah dihapus."});
            }
        });
    }
};

model.disableOldUmroh = function(callback) {
    var statement = "UPDATE Hasanah AS doc SET doc.status=0 " 
            + "WHERE DATE_DIFF_STR(DATE_ADD_STR(now_str(), 30, 'day'), doc.departure, 'day') > 30";
    var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
    db.query(query, function(err, result) {
        if (err) {
            callback(err, null);
        } else {
            callback(null, result);
        }
    });
};

model.getById = function(req, callback) {
    var id = req.body.id;
    
    log.info("modelUmroh.getbyid : " + id);
    
    if (!id) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        db.get(id, function(err, result) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, result.value);
            }
        });
    }
};

model.getAll = function(req, callback) {
    var status = req.body.status;
    var limit = req.body.limit;
    var page = req.body.page;
    var offset = 0;
    var filter = "";
    
    if (!limit) {
        limit = 10;
    }
    if (page && page>0) {
        offset = (page - 1) * limit;
    }
    if (status) {
        filter = " AND status=" + status;
    }
    
    var statement = "SELECT META(doc).id, name, img, city, departure, price, packet, " 
            + "phone, plane, hotel, quota, content, status "
            + "FROM " + dbConfig.dbName + " AS doc "
            + "WHERE type='" + documentType + "'" + filter + " ORDER BY doc.id LIMIT " + limit + " OFFSET " + offset;
    var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
    db.query(query, function(err, result) {
        if (err) {
            callback(err, null);
        } else {
            callback(null, result);
        }
    });
};

model.getPlane = function(req, callback) {
    var statement = "SELECT DISTINCT plane "
            + "FROM " + dbConfig.dbName + " AS doc "
            + "WHERE type='" + documentType + "' ORDER BY plane";
    var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
    db.query(query, function(err, result) {
        if (err) {
            callback(err, null);
        } else {
            callback(null, result);
        }
    });
};

model.getActive = function(req, callback) {
    var limit = req.body.limit;
    var page = req.body.page;
    var offset = 0;
    
    if (!limit) {
        limit = 10;
    }
    if (page && page>0) {
        offset = (page - 1) * limit;
    }
    
    var statement = "SELECT META(doc).id, name, img, city, departure, price, packet, " 
            + "phone, plane, hotel, quota, content, status "
            + "FROM " + dbConfig.dbName + " AS doc "
            + "WHERE type='" + documentType + "' AND status=1 ORDER BY doc.id LIMIT " + limit + " OFFSET " + offset;
    var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
    db.query(query, function(err, result) {
        if (err) {
            callback(err, null);
        } else {
            callback(null, result);
        }
    });
};

model.getCount = function(req, callback) {
    var city = req.body.city;
    var packet = req.body.packet;
    var plane = req.body.plane;
    var hotel = req.body.hotel;
    var dateStart = req.body.date_start;
    var dateEnd =  req.body.date_end;
    var minPrice = req.body.min_price;
    var maxPrice = req.body.max_price;
    var status = req.body.status;
    var filter = "";
    
    if (status) {
        filter = filter + " AND status=" + status;
    }
    if (city) {
        filter = filter + " AND LOWER(city) LIKE '%" + city.toLowerCase() + "%'";
    }
    if (packet) {
        filter = filter + " AND LOWER(packet)='" + packet.toLowerCase() + "'";
    }
    if (plane) {
        filter = filter + " AND LOWER(plane)='" + plane.toLowerCase() + "'";
    }
    if (hotel) {
        filter = filter + " AND hotel=" + hotel;
    }
    if (minPrice && maxPrice) {
        filter = filter + " AND (price>=" + minPrice + " AND price<=" + maxPrice + ")";
    }
    if (dateStart && dateEnd) {
        filter = filter + " AND departure BETWEEN '" 
                + new Date(dateStart).format('yyyy-mm-dd HH:MM:ss') + "' AND '" 
                + new Date(dateEnd).format('yyyy-mm-dd HH:MM:ss') + "'";
    }
    
    var statement = "SELECT COUNT(META(doc).id) AS total "
            + "FROM " + dbConfig.dbName + " doc "
            + "WHERE doc.type='" + documentType + "'" + filter;

    var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
    db.query(query, function(err, result) {
        if (err) {
            log.error("modelArticle.getCount : " + err);
            callback(err, null);
        } else {
            callback(null, result);
        }
    });
};

model.search = function(req, callback) {
    var city = req.body.city;
    var packet = req.body.packet;
    var plane = req.body.plane;
    var hotel = req.body.hotel;
    var dateStart = req.body.date_start;
    var dateEnd =  req.body.date_end;
    var minPrice = req.body.min_price;
    var maxPrice = req.body.max_price;
    var status = req.body.status;
    
    log.info("modelUmroh.search : city - " + city);
    log.info("modelUmroh.search : packet - " + packet);
    log.info("modelUmroh.search : plane - " + plane);
    log.info("modelUmroh.search : hotel - " + hotel);
    log.info("modelUmroh.search : dateStart - " + dateStart);
    log.info("modelUmroh.search : dateEnd - " + dateEnd);
    log.info("modelUmroh.search : minPrice - " + minPrice);
    log.info("modelUmroh.search : maxPrice - " + maxPrice);
    log.info("modelUmroh.search : status - " + status);
    
    var limit = req.body.limit;
    var page = req.body.page;
    var offset = 0;
    var filter = "";
    
    if (!limit) {
        limit = 10;
    }
    if (page && page>0) {
        offset = (page - 1) * limit;
    }
    if (status) {
        filter = filter + " AND status=" + status;
    }
    if (city) {
        filter = filter + " AND LOWER(city) LIKE '%" + city.toLowerCase() + "%'";
    }
    if (packet) {
        filter = filter + " AND LOWER(packet)='" + packet.toLowerCase() + "'";
    }
    if (plane) {
        filter = filter + " AND LOWER(plane)='" + plane.toLowerCase() + "'";
    }
    if (hotel) {
        filter = filter + " AND hotel=" + hotel;
    }
    if (minPrice && maxPrice) {
        filter = filter + " AND (price>=" + minPrice + " AND price<=" + maxPrice + ")";
    }
    if (dateStart && dateEnd) {
        filter = filter + " AND departure BETWEEN '" 
                + new Date(dateStart).format('yyyy-mm-dd HH:MM:ss') + "' AND '" 
                + new Date(dateEnd).format('yyyy-mm-dd HH:MM:ss') + "'";
    }
    var statement = "SELECT META(doc).id, name, img, city, departure, price, packet, " 
            + "phone, plane, hotel, quota, content, status "    
            + "FROM " + dbConfig.dbName + " AS doc " 
            + "WHERE doc.type='" + documentType + "'" + filter + " ORDER BY doc.id LIMIT " + limit + " OFFSET " + offset;
    
    log.info("modelUmroh.search : statement - " + statement);
    
    var query = queryFactory.fromString(statement);
    db.query(query, function(err, result) {
        if (err) {
            log.error("modelUmroh.search : " + err);
            callback(err, null);
        } else {
            log.info("modelUmroh.search : result - " + result);
            callback(null, result);
        }
    });
};

module.exports = model;