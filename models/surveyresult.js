var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
require('x-date');
var dbConfig = require('../config/dbConfig');
var db = dbConfig.getDb();
var queryFactory = dbConfig.getQueryFactory();
var documentType = "";


function model(docType) {
    documentType = docType;
};

model.save = function(req, callback) {
    var documentID = req.body.id;
    var idQuestion = req.body.id_question;
    var idDevice = req.body.uuid;
    
    var jsonData = {
        type: documentType,
        uuid: idDevice,
        device: req.body.device,
        input_time: new Date().format('yyyy-mm-dd HH:MM:ss'),
        id_question: idQuestion,
        answer: req.body.answer
    };

    log.info("modelUmroh.save : " + JSON.stringify(jsonData));
    
    var add = false;

    if (!documentID) {
        add = true;
        documentID = new Date().format('yyyymmddHHMMss') + uuid.v4();
    }

    if (add) {
        var statement = "SELECT META(doc).id, uuid, device, input_time, id_question, answer "
                + "FROM " + dbConfig.dbName + " AS doc "
                + "WHERE type='" + documentType + "' AND uuid='" + idDevice + "' AND id_question='" + idQuestion + "' LIMIT 1";
        var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
        db.query(query, function(err, result) {
            if (err) {
                callback(err, null);
            } else {
                if (result.id) {
                    callback({status: "error", message: "Maaf, anda telah mengisi survey ini sebelumnya.", id: documentID}, null);
                } else {
                    db.upsert(documentID, jsonData, function(error, result) {
                        if (error) {
                            callback(error, null);
                        } else {
                            callback(null, {status: "success", message: "Data telah disimpan.", id: documentID});
                        }
                    });
                }
            }
        });
    } else {
        db.upsert(documentID, jsonData, function(error, result) {
            if (error) {
                callback(error, null);
            } else {
                callback(null, {status: "success", message: "Data telah diubah.", id: documentID});
            }
        });
    }
};

model.delete = function(req, callback) {
    var id = req.body.id;
    
    if (!id) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        db.remove(id, function (err, res) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, {status: "success", message: "Data telah dihapus."});
            }
        });
    }
};

model.getById = function(req, callback) {
    var id = req.body.id;
    
    if (!id) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        db.get(id, function(err, result) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, result.value);
            }
        });
    }
};

model.getAll = function(req, callback) {
    var statement = "SELECT META(doc).id, uuid, device, input_time, id_question, answer "
            + "FROM " + dbConfig.dbName + " AS doc "
            + "WHERE type='" + documentType + "' ORDER BY doc.id";
    var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
    db.query(query, function(err, result) {
        if (err) {
            callback(err, null);
        } else {
            callback(null, result);
        }
    });
};

model.search = function(req, callback) {
    var key = req.body.key;
    
    if (!key) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        var statement = "SELECT META(doc).id, uuid, device, input_time, id_question, answer "
                + "FROM " + dbConfig.dbName + " AS doc " 
                + "WHERE answer LIKE '%$1%' AND doc.type='" + documentType + "'";
        var query = queryFactory.fromString(statement);
        db.query(query, [key], function(err, result) {
            if(err) {
                callback(err, null);
            } else {
                callback(null, result);
            }
        });
    }
};

module.exports = model;