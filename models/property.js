var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
require('x-date');
var fs = require('fs');
var appConfig = require('../config/appConfig');
var dbConfig = require('../config/dbConfig');
var db = dbConfig.getDb();
var queryFactory = dbConfig.getQueryFactory();
var documentType = "";


function model(docType) {
    documentType = docType;
};

model.save = function(req, callback) {
    var documentID = req.body.id;
    
    var jsonData = {
        type: documentType,
        name: req.body.name,
        price: req.body.price,
        city: req.body.city,
        address: req.body.address,
        location: req.body.location,
        surface_area: req.body.surface_area,
        building_area: req.body.building_area,
        auction_date: new Date(req.body.auction_date),
        img: req.body.img,
        info: req.body.info,
        status: req.body.status
    };

    var add = false;

    if (!documentID) {
        add = true;
        documentID = new Date().format('yyyymmddHHMMss') + uuid.v4();
    }

    db.upsert(documentID, jsonData, function(error, result) {
        if (error) {
            callback(error, null);
        } else {
            if (add) {
                callback(null, {status: "success", message: "Data telah disimpan.", id: documentID});
            } else {
                callback(null, {status: "success", message: "Data telah diubah.", id: documentID});
            }
        }
    });
};

model.delete = function(req, callback) {
    var id = req.body.id;
    
    if (!id) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        db.remove(id, function (err, res) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, {status: "success", message: "Data telah dihapus."});
            }
        });
    }
};

model.getById = function(req, callback) {
    var id = req.body.id;
    
    if (!id) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        db.get(id, function(err, result) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, result.value);
            }
        });
    }
};

model.getAll = function(req, callback) {
    var status = req.body.status;
    var limit = req.body.limit;
    var page = req.body.page;
    var offset = 0;
    var filter = "";
    
    if (!limit) {
        limit = 10;
    }
    if (page && page>0) {
        offset = (page - 1) * limit;
    }
    if (status) {
        filter = " AND status=" + status;
    }
    
    var statement = "SELECT META(doc).id, name, price, city, address, location, surface_area, building_area, auction_date, img, info, status "
            + "FROM " + dbConfig.dbName + " AS doc "
            + "WHERE type='" + documentType + "'" + filter + " ORDER BY doc.id LIMIT " + limit + " OFFSET " + offset;
    var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
    db.query(query, function(err, result) {
        if (err) {
            callback(err, null);
        } else {
            for(var property of result) {
                if (property.img !== []) {
                    property.img = [property.img[0]];
                }
            }
            callback(null, result);
        }
    });
};

model.getCount = function(req, callback) {
    var city = req.body.city;
    var location = req.body.location;
    var minPrice = req.body.price_min;
    var maxPrice = req.body.price_max;
    var minSurfaceArea = req.body.surface_area_min;
    var maxSurfaceArea = req.body.surface_area_max;
    var minBuildingArea = req.body.building_area_min;
    var maxBuildingArea = req.body.building_area_max;
    var status = req.body.status;
    var filter = "";
    
    if (status) {
        filter = " AND status=" + status;
    }
    if (city) {
        filter = filter + " AND LOWER(city) LIKE '%" + city.toLowercase() + "%'";
    }
    if (location) {
        filter = filter + " AND LOWER(location) LIKE '%" + location.toLowerCase() + "%'";
    }
    if (minPrice && maxPrice) {
        filter = filter + " AND (price>=" + minPrice + " AND price<=" + maxPrice + ")";
    }
    if (minSurfaceArea && maxSurfaceArea) {
        filter = filter + " AND (surface_area>=" + minSurfaceArea + " AND surface_area<=" + maxSurfaceArea + ")";
    }
    if (minBuildingArea && maxBuildingArea) {
        filter = filter + " AND (building_area>=" + minBuildingArea + " AND building_area<=" + maxBuildingArea + ")";
    }
    
    var statement = "SELECT COUNT(META(doc).id) AS total "
            + "FROM " + dbConfig.dbName + " doc "
            + "WHERE doc.type='" + documentType + "'" + filter;

    var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
    db.query(query, function(err, result) {
        if (err) {
            log.error("modelArticle.getCount : " + err);
            callback(err, null);
        } else {
            callback(null, result);
        }
    });
};

model.search = function(req, callback) {
    var city = req.body.city;
    var location = req.body.location;
    var minPrice = req.body.price_min;
    var maxPrice = req.body.price_max;
    var minSurfaceArea = req.body.surface_area_min;
    var maxSurfaceArea = req.body.surface_area_max;
    var minBuildingArea = req.body.building_area_min;
    var maxBuildingArea = req.body.building_area_max;
    var status = req.body.status;
    var limit = req.body.limit;
    var page = req.body.page;
    var offset = 0;
    var filter = "";
    
    if (!limit) {
        limit = 10;
    }
    if (page && page>0) {
        offset = (page - 1) * limit;
    }
    if (status) {
        filter = " AND status=" + status;
    }
    if (city) {
        filter = filter + " AND LOWER(city) LIKE '%" + city.toLowercase() + "%'";
    }
    if (location) {
        filter = filter + " AND LOWER(location) LIKE '%" + location.toLowerCase() + "%'";
    }
    if (minPrice && maxPrice) {
        filter = filter + " AND (price>=" + minPrice + " AND price<=" + maxPrice + ")";
    }
    if (minSurfaceArea && maxSurfaceArea) {
        filter = filter + " AND (surface_area>=" + minSurfaceArea + " AND surface_area<=" + maxSurfaceArea + ")";
    }
    if (minBuildingArea && maxBuildingArea) {
        filter = filter + " AND (building_area>=" + minBuildingArea + " AND building_area<=" + maxBuildingArea + ")";
    }
    
    var statement = "SELECT META(doc).id, name, price, city, address, location, surface_area, building_area, auction_date, img, info, status "
            + "FROM " + dbConfig.dbName + " AS doc " 
            + "WHERE doc.type='" + documentType + "'" + filter + " LIMIT " + limit + " OFFSET " + offset;
    
    log.info("modelProperty.search : statement - " + statement);
    
    var query = queryFactory.fromString(statement);
    db.query(query, function(err, result) {
        if(err) {
            callback(err, null);
        } else {
            for(var property of result) {
                if (property.img !== []) {
                    property.img = [property.img[0]];
                }
            }
            callback(null, result);
        }
    });
};

module.exports = model;