var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
require('x-date');
var fs = require('fs');
var appConfig = require('../config/appConfig');
var dbConfig = require('../config/dbConfig');
var db = dbConfig.getDb();
var queryFactory = dbConfig.getQueryFactory();
var documentType = "";


function model(docType) {
    documentType = docType;
};

model.save = function(req, callback) {
    var documentID = req.body.id;
    var description = req.body.description;
    
    if (!description) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        var jsonData = {
            type: documentType,
            category: req.body.category,
            name: req.body.name,
            description: description,
            img: req.body.img,
            content: req.body.content,
            status: req.body.status
        };

        var add = false;

        if (!documentID) {
            add = true;
            documentID = new Date().format('yyyymmddHHMMss') + uuid.v4();
        }

        db.upsert(documentID, jsonData, function(error, result) {
            if (error) {
                callback(error, null);
            } else {
                if (add) {
                    callback(null, {status: "success", message: "Data telah disimpan.", id: documentID});
                } else {
                    callback(null, {status: "success", message: "Data telah diubah.", id: documentID});
                }
            }
        });
    }
};

model.delete = function(req, callback) {
    var id = req.body.id;
    
    if (!id) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        db.remove(id, function (err, res) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, {status: "success", message: "Data telah dihapus."});
            }
        });
    }
};

model.getById = function(req, callback) {
    var id = req.body.id;
    
    if (!id) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        db.get(id, function(err, result) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, result.value);
            }
        });
    }
};

model.getByCategory = function(req, callback) {
    var category = req.body.category;
    
    if (!category) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        var statement = "SELECT META(doc).id, category, name, description, img, content, status "
            + "FROM " + dbConfig.dbName + " AS doc "
            + "WHERE type='" + documentType + "' AND category='" + category + "' ORDER BY doc.id";
        var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
        db.query(query, function(err, result) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, result);
            }
        });
    }
};

model.getAll = function(req, callback) {
    var status = req.body.status;
    var category = req.body.category;
    var filter = "";
    
    if (status) {
        filter = " AND status=" + status;
    }
    if (category) {
        filter = " AND category='" + category + "'";
    }
    
    var statement = "SELECT META(doc).id, category, name, description, img, content, status "
            + "FROM " + dbConfig.dbName + " AS doc "
            + "WHERE type='" + documentType + "'" + filter + " ORDER BY doc.id";
    var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
    db.query(query, function(err, result) {
        if (err) {
            callback(err, null);
        } else {
            callback(null, result);
        }
    });
};

model.search = function(req, callback) {
    var key = req.body.key;
    var status = req.body.status;
    var category = req.body.category;
    var filter = "";
    
    if (status) {
        filter = " AND status=" + status;
    }
    if (category) {
        filter = " AND category='" + category + "'";
    }
    
    var statement = "SELECT META(doc).id, category, name, description, img, content, status "
            + "FROM " + dbConfig.dbName + " AS doc " 
            + "WHERE description LIKE '%" + key + "%' AND doc.type='" + documentType 
            + "'" + filter + " ORDER BY doc.id";
    var query = queryFactory.fromString(statement);
    db.query(query, function(err, result) {
        if(err) {
            callback(err, null);
        } else {
            callback(null, result);
        }
    });
};

module.exports = model;