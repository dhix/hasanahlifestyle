var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
require('x-date');
var fs = require('fs');
var appConfig = require('../config/appConfig');
var dbConfig = require('../config/dbConfig');
var db = dbConfig.getDb();
var queryFactory = dbConfig.getQueryFactory();
var documentType = "";


function model(docType) {
    documentType = docType;
};

model.save = function(req, callback) {
    var documentID = req.body.id;
    var email = req.body.email;
    var pass = req.body.pass;
    
    if (!email || !pass) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        var jsonData = {
            type: documentType,
            img: req.body.img,
            name: req.body.name,
            email: email,
            pass: pass,
            user_type: req.body.user_type.toUpperCase(),
            menu: req.body.menu,
            aprove: req.body.aprove,
            description: req.body.description
        };

        var add = false;
        var onRow = false;
        
        if (!documentID) {
            add = true;
            documentID = new Date().format('yyyymmddHHMMss') + uuid.v4();
        }

        var statement = "SELECT META(doc).id, img, name, email, pass, user_type, menu, aprove, description "
                + "FROM " + dbConfig.dbName + " AS doc "
                + "WHERE doc.email='" + email + "' AND type='" + documentType + "' LIMIT 1";

        var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
        var dbReq = db.query(query);
        dbReq.on('row', function(row) {
            onRow = true;
            if (add) {
                callback({status: "success", message: "Email telah terdaftar."}, null);
            } else if (row.id !== documentID) {
                callback({status: "success", message: "Email telah terdaftar."}, null);
            } else {
                db.upsert(documentID, jsonData, function(error, result) {
                    if (error) {
                        callback(error, null);
                    } else {
                        callback(null, {status: "success", message: "Data telah diubah."});
                    }
                });
            }
        });
        dbReq.on('end', function(row) {
            if (!onRow) {
                db.upsert(documentID, jsonData, function(error, result) {
                    if (error) {
                        callback(error, null);
                    } else {
                        callback(null, {status: "success", message: "Data telah disimpan."});
                    }
                });
            }
        });
        dbReq.on('error', function(err) {
            db.upsert(documentID, jsonData, function(error, result) {
                if (error) {
                    callback(error, null);
                } else {
                    if (add) {
                        callback(null, {status: "success", message: "Data telah disimpan.", id: documentID});
                    } else {
                        callback(null, {status: "success", message: "Data telah diubah.", id: documentID});
                    }
                }
            });
        });
    }
};

model.delete = function(req, callback) {
    var id = req.body.id;
    
    if (!id) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        db.get(id, function(err, result) {
            if (err) {
                callback(err, null);
            } else {
                db.remove(id, function (err, res) {
                    if (err) {
                        callback(err, null);
                    } else {
                        var img = result.value.img;
                        if (!img) {
                            callback(null, {status: "success", message: "Data telah dihapus."});
                        } else {
                            img = img.replace(appConfig.SERVER_ADDRESS, "");
                            fs.exists('./public/images/' + img, function(exists) {
                                if (exists) {
                                    fs.unlink('./public/images/' + img);
                                }
                            });
                            callback(null, {status: "success", message: "Data telah dihapus."});
                        }
                    }
                });
            }
        });
    }
};

model.getById = function(req, callback) {
    var id = req.body.id;
    
    if (!id) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        db.get(id, function(err, result) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, result.value);
            }
        });
    }
};

model.getAll = function(req, callback) {
    var statement = "SELECT META(doc).id, img, name, email, pass, user_type, menu, aprove, description "
            + "FROM " + dbConfig.dbName + " AS doc "
            + "WHERE type='" + documentType + "' ORDER BY doc.id";
    
    var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
    db.query(query, function(err, result) {
        if (err) {
            callback(err, null);
        } else {
            callback(null, result);
        }
    });
};

model.getByEmail = function(req, callback) {
    var email = req.body.email;
    
    if (!email) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        var notFound = true;
        
        var statement = "SELECT META(doc).id, img, name, email, pass, user_type, menu, aprove, description "
                + "FROM " + dbConfig.dbName + " AS doc "
                + "WHERE doc.email='" + email + "' AND type='" + documentType + "' LIMIT 1";

        var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
        var dbReq = db.query(query);
        dbReq.on('row', function(row) {
            notFound = false;
            callback(null, row);
        });
        dbReq.on('error', function(err) {
            callback(err, null);
        });
        dbReq.on('end', function(result) {
            if (notFound===true) {
                callback({status: "error", message: "Email tidak terdaftar."}, null);
            }
        });
    }
};

model.search = function(req, callback) {
    var key = req.body.key;
    
    if (!key) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        var statement = "SELECT META(doc).id, img, name, email, pass, user_type, menu, aprove, description "
                + "FROM " + dbConfig.dbName + " AS doc " 
                + "WHERE description LIKE '%$1%' AND doc.type='" + documentType + "'";
        var query = queryFactory.fromString(statement);
        db.query(query, [key], function(err, result) {
            if(err) {
                callback(err, null);
            } else {
                callback(null, result);
            }
        });
    }
};

module.exports = model;