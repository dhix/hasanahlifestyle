var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
require('x-date');
var appConfig = require('../config/appConfig');
var dbConfig = require('../config/dbConfig');
var db = dbConfig.getDb();
var queryFactory = dbConfig.getQueryFactory();
var documentType = "";


function model(docType) {
    documentType = docType;
};

model.save = function(req, callback) {
    var documentID = req.body.id;
    
    var jsonData = {
        type: documentType,
        name: req.body.name
    };

    var add = false;

    if (!documentID) {
        add = true;
        documentID = new Date().format('yyyymmddHHMMss') + uuid.v4();
    }

    db.upsert(documentID, jsonData, function(error, result) {
        if (error) {
            callback(error, null);
        } else {
            if (add) {
                callback(null, {status: "success", message: "Data telah disimpan.", id: documentID});
            } else {
                callback(null, {status: "success", message: "Data telah diubah.", id: documentID});
            }
        }
    });
};

model.delete = function(req, callback) {
    var id = req.body.id;
    
    if (!id) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        db.remove(id, function (err, res) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, {status: "success", message: "Data telah dihapus."});
            }
        });
    }
};

model.getById = function(req, callback) {
    var id = req.body.id;
    
    if (!id) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        db.get(id, function(err, result) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, result.value);
            }
        });
    }
};

model.getAll = function(req, callback) {
    var limit = req.body.limit;
    var page = req.body.page;
    var offset = 0;
    
    if (!limit) {
        limit = 10;
    }
    if (page && page>0) {
        offset = (page - 1) * limit;
    }
    
    var statement = "SELECT META(doc).id, name "
            + "FROM " + dbConfig.dbName + " AS doc "
            + "WHERE type='" + documentType + "' ORDER BY doc.name LIMIT " + limit + " OFFSET " + offset;
    var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
    db.query(query, function(err, result) {
        if (err) {
            callback(err, null);
        } else {
            callback(null, result);
        }
    });
};

model.getAllCity = function(req, callback) {
    var statement = "SELECT META(doc).id, name "
            + "FROM " + dbConfig.dbName + " AS doc "
            + "WHERE type='" + documentType + "' ORDER BY doc.name";
    var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
    db.query(query, function(err, result) {
        if (err) {
            callback(err, null);
        } else {
            callback(null, result);
        }
    });
};

model.getCount = function(req, callback) {
    var key = req.body.key;
    var filter = "";
    
    if (key) {
        key = key.toLowerCase();
        filter = filter + " AND LOWER(name) LIKE '%" + key + "%'";
    }
    
    var statement = "SELECT COUNT(META(doc).id) AS total "
            + "FROM " + dbConfig.dbName + " doc "
            + "WHERE doc.type='" + documentType + "'" + filter;

    var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
    db.query(query, function(err, result) {
        if (err) {
            log.error("modelArticle.getCount : " + err);
            callback(err, null);
        } else {
            callback(null, result);
        }
    });
};

model.search = function(req, callback) {
    var key = req.body.key;
    var filter = "";
    
    if (key) {
        key = key.toLowerCase();
        filter = filter + " AND LOWER(name) LIKE '%" + key + "%'";
    }
    
    var statement = "SELECT META(doc).id, name "
            + "FROM " + dbConfig.dbName + " AS doc " 
            + "WHERE doc.type='" + documentType + "'" + filter + " ORDER BY doc.name";
    var query = queryFactory.fromString(statement);
    db.query(query, function(err, result) {
        if(err) {
            callback(err, null);
        } else {
            callback(null, result);
        }
    });
};

module.exports = model;