var log = require('../util/logger.js').LOG;
var dbConfig = require('../config/dbConfig');
var db = dbConfig.getDb();
var queryFactory = dbConfig.getQueryFactory();
var documentType = "";

function model(docType) {
    documentType = docType;
};

model.save = function(req, callback) {
    var documentID = req.body.id;
    
    if (!documentID) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        var jsonData = {
            type: documentType,
            email: req.body.email,
            title: req.body.title,
            content: req.body.content
        };
        
        db.upsert(documentID, jsonData, function(error, result) {
            if (error) {
                callback(error, null);
            } else {
                callback(null, {status: "success", message: "Data telah diubah."});
            }
        });
    }
};

model.getAll = function(req, callback) {
    var notFound = true;
    
    var statement = "SELECT META(doc).id, email, title, content "
            + "FROM " + dbConfig.dbName + " AS doc "
            + "WHERE type='" + documentType + "' LIMIT 1";
    var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
    var dbReq = db.query(query);
    dbReq.on('row', function(row) {
        notFound = false;
        callback(null, row);
    });
    dbReq.on('error', function(err) {
        callback(err, null);
    });
    dbReq.on('end', function(result) {
        if (notFound===true) {
            callback({status: "error", message: "Data tidak ditemukan."}, null);
        }
    });
};

module.exports = model;