var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
var mail = require('../util/mail.js');
require('x-date');
var dbConfig = require('../config/dbConfig');
var db = dbConfig.getDb();
var queryFactory = dbConfig.getQueryFactory();
var documentType = "";


function model(docType) {
    documentType = docType;
};

model.save = function(req, callback) {
    var documentID = req.body.id;
    var date = new Date().format('yyyy-mm-dd HH:MM:ss');
    
    var jsonData = {
        type: documentType,
        date: date,
        name: req.body.name,
        address: req.body.address,
        gender: req.body.gender,
        phone: req.body.phone,
        nearest_branch_id: req.body.nearest_branch_id,
        type_id: req.body.type_id
    };

    var add = false;

    if (!documentID) {
        add = true;
        documentID = new Date().format('yyyymmddHHMMss') + uuid.v4();
        
        mail.send("propertysetting", function(result) {});
    }

    db.upsert(documentID, jsonData, function(error, result) {
        if (error) {
            callback(error, null);
        } else {
            if (add) {
                callback(null, {status: "success", message: "Data telah disimpan.", id: documentID});
            } else {
                callback(null, {status: "success", message: "Data telah diubah.", id: documentID});
            }
        }
    });
};

model.delete = function(req, callback) {
    var id = req.body.id;
    
    if (!id) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        db.remove(id, function (err, res) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, {status: "success", message: "Data telah dihapus."});
            }
        });
    }
};

model.getById = function(req, callback) {
    var id = req.body.id;
    
    if (!id) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        var statement = "SELECT * FROM Hasanah propertyregister " 
                + "JOIN Hasanah property ON KEYS propertyregister.type_id "
                + "JOIN Hasanah branch ON KEYS propertyregister.nearest_branch_id "
                + "WHERE propertyregister.id='" + id + "'";
        var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
        db.query(query, function(err, result) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, result);
            }
        });
    }
};

model.getAll = function(req, callback) {
    var statement = "SELECT * FROM Hasanah propertyregister " 
            + "JOIN Hasanah property ON KEYS propertyregister.type_id "
            + "JOIN Hasanah branch ON KEYS propertyregister.nearest_branch_id "
            + "WHERE propertyregister.type='" + documentType + "' ORDER BY propertyregister.id";
    var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
    db.query(query, function(err, result) {
        if (err) {
            callback(err, null);
        } else {
            callback(null, result);
        }
    });
};

model.getReport = function(req, callback) {
    var dateStart = req.body.date_start;
    var dateEnd = req.body.date_end;
    
    var filter = "";
    if (dateStart && dateEnd) {
        filter = filter + " AND propertyregister.date BETWEEN '" + dateStart + "' AND '" + dateEnd + "'";
    }
    
    var statement = "SELECT * FROM Hasanah propertyregister " 
            + "JOIN Hasanah property ON KEYS propertyregister.type_id "
            + "JOIN Hasanah branch ON KEYS propertyregister.nearest_branch_id "
            + "WHERE propertyregister.type='" + documentType + "'" + filter + " ORDER BY propertyregister.id";
    var query = queryFactory.fromString(statement);
    db.query(query, function(err, result) {
        if(err) {
            callback(err, null);
        } else {
            callback(null, result);
        }
    });
};

model.search = function(req, callback) {
    var key = req.body.key;
    
    if (!key) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        var statement = "SELECT * FROM Hasanah propertyregister " 
            + "JOIN Hasanah property ON KEYS propertyregister.type_id "
            + "JOIN Hasanah branch ON KEYS propertyregister.nearest_branch_id "
            + "WHERE propertyregister.name LIKE '%$1%' AND propertyregister.type='" + documentType + "' " 
            + "ORDER BY propertyregister.id";
        var query = queryFactory.fromString(statement);
        db.query(query, [key], function(err, result) {
            if(err) {
                callback(err, null);
            } else {
                callback(null, result);
            }
        });
    }
};

module.exports = model;