var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
require('x-date');
var dbConfig = require('../config/dbConfig');
var db = dbConfig.getDb();
var queryFactory = dbConfig.getQueryFactory();
var documentType = "";


function model(docType) {
    documentType = docType;
};

model.save = function(req, callback) {
    var documentID = req.body.id;
    var date = new Date().format('yyyy-mm-dd HH:MM:ss');
    
    var jsonData = {
        type: documentType,
        date: date,
        name: req.body.name,
        address: req.body.address,
        birthday: req.body.birthday,
        business_field: req.body.business_field,
        city: req.body.city,
        company_type: req.body.company_type,
        contact_to: req.body.contact_to,
        email: req.body.email,
        facility: req.body.facility,
        gender: req.body.gender,
        income: req.body.income,
        job: req.body.job,
        job_position: req.body.job_position,
        kecamatan: req.body.kecamatan,
        kelurahan: req.body.kelurahan,
        length: req.body.length,
        nearest_branch_id: req.body.nearest_branch_id,
        office_name: req.body.office_name,
        office_phone :req.body.office_phone,
        phone: req.body.phone,
        postal_code: req.body.postal_code,
        rt: req.body.rt,
        rw: req.body.rw,
        service_length: req.body.service_length,
        type_id: req.body.type_id,
        year_start_work: req.body.year_start_work
    };

    var add = false;

    if (!documentID) {
        add = true;
        documentID = new Date().format('yyyymmddHHMMss') + uuid.v4();
    }

    db.upsert(documentID, jsonData, function(error, result) {
        if (error) {
            callback(error, null);
        } else {
            if (add) {
                callback(null, {status: "success", message: "Data telah disimpan.", id: documentID});
            } else {
                callback(null, {status: "success", message: "Data telah diubah.", id: documentID});
            }
        }
    });
};

model.delete = function(req, callback) {
    var id = req.body.id;
    
    if (!id) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        db.remove(id, function (err, res) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, {status: "success", message: "Data telah dihapus."});
            }
        });
    }
};

model.getById = function(req, callback) {
    var id = req.body.id;
    
    if (!id) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        var statement = "SELECT * FROM Hasanah financefiling " 
                + "JOIN Hasanah finance ON KEYS financefiling.type_id "
                + "JOIN Hasanah branch ON KEYS financefiling.nearest_branch_id "
                + "WHERE financefiling.id='" + id + "'";
        var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
        db.query(query, function(err, result) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, result);
            }
        });
    }
};

model.getAll = function(req, callback) {
    var statement = "SELECT * FROM Hasanah financefiling " 
            + "JOIN Hasanah finance ON KEYS financefiling.type_id "
            + "JOIN Hasanah branch ON KEYS financefiling.nearest_branch_id "
            + "WHERE financefiling.type='" + documentType + "' ORDER BY financefiling.id";
    var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
    db.query(query, function(err, result) {
        if (err) {
            callback(err, null);
        } else {
            callback(null, result);
        }
    });
};

model.getReport = function(req, callback) {
    var dateStart = req.body.date_start;
    var dateEnd = req.body.date_end;
    
    log.info("modelFinancefiling.search : dateStart - " + dateStart);
    log.info("modelFinancefiling.search : dateEnd - " + dateEnd);
    
    var filter = "";
    if (dateStart && dateEnd) {
        filter = filter + " AND financefiling.date BETWEEN '" + dateStart + "' AND '" + dateEnd + "'";
    }
    
    var statement = "SELECT * FROM Hasanah financefiling " 
            + "JOIN Hasanah finance ON KEYS financefiling.type_id "
            + "JOIN Hasanah branch ON KEYS financefiling.nearest_branch_id "
            + "WHERE financefiling.type='" + documentType + "'" + filter + " ORDER BY financefiling.id";
    
    log.info("modelFinancefiling.search : statement - " + statement);
    
    var query = queryFactory.fromString(statement);
    db.query(query, function(err, result) {
        if(err) {
            callback(err, null);
        } else {
            callback(null, result);
        }
    });
};

model.search = function(req, callback) {
    var key = req.body.key;
    
    if (!key) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        var statement = "SELECT * FROM Hasanah financefiling " 
            + "JOIN Hasanah finance ON KEYS financefiling.type_id "
            + "JOIN Hasanah branch ON KEYS financefiling.nearest_branch_id "
            + "WHERE financefiling.name LIKE '%$1%' AND financefiling.type='" + documentType + "' " 
            + "ORDER BY financefiling.id";
        var query = queryFactory.fromString(statement);
        db.query(query, [key], function(err, result) {
            if(err) {
                callback(err, null);
            } else {
                callback(null, result);
            }
        });
    }
};

module.exports = model;