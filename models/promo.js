var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
require('x-date');
var dbConfig = require('../config/dbConfig');
var db = dbConfig.getDb();
var queryFactory = dbConfig.getQueryFactory();
var documentType = "";

function model(docType) {
    documentType = docType;
};

model.save = function(req, callback) {
    var documentID = req.body.id;
    var description = req.body.description;
    
    if (!description) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        var jsonData = {
            type: documentType,
            img: req.body.img,
            description: description,
            content: req.body.content,
            status: req.body.status
        };

        var documentID = req.body.id;
        var add = false;

        if (!documentID) {
            add = true;
            documentID = new Date().format('yyyymmddHHMMss') + uuid.v4();
        }

        db.upsert(documentID, jsonData, function(error, result) {
            if (error) {
                callback(error, null);
            } else {
                if (add) {
                    callback(null, {status: "success", message: "Data telah disimpan.", id: documentID});
                } else {
                    callback(null, {status: "success", message: "Data telah diubah.", id: documentID});
                }
            }
        });
    }
};

model.delete = function(req, callback) {
    var id = req.body.id;
    
    if (!id) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        db.remove(id, function (err, res) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, {status: "success", message: "Data telah dihapus."});
            }
        });
    }
};

model.getById = function(req, callback) {
    var id = req.body.id;
    
    if (!id) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        db.get(id, function(err, result) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, result.value);
            }
        });
    }
};

model.getAll = function(req, callback) {
    var status = req.body.status;
    var limit = req.body.limit;
    var page = req.body.page;
    var offset = 0;
    var filter = "";
    
    if (!limit) {
        limit = 10;
    }
    if (page && page>0) {
        offset = (page - 1) * limit;
    }
    if (status) {
        filter = " AND status=" + status;
    }
    
    var statement = "SELECT META(doc).id, img, description, content, status "
            + "FROM " + dbConfig.dbName + " AS doc "
            + "WHERE type='" + documentType + "'" + filter + " ORDER BY doc.id LIMIT " + limit + " OFFSET " + offset;
    var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
    db.query(query, function(err, result) {
        if (err) {
            callback(err, null);
        } else {
            callback(null, result);
        }
    });
};

model.getActivePromo = function(req, callback) {
    var statement = "SELECT META(doc).id, img, description, content, status "
            + "FROM " + dbConfig.dbName + " AS doc "
            + "WHERE status=1 AND type='" + documentType + "' ORDER BY doc.id";
    var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
    db.query(query, function(err, result) {
        if (err) {
            callback(err, null);
        } else {
            callback(null, result);
        }
    });
};

model.getCount = function(req, callback) {
    var key = req.body.key;
    var status = req.body.status;
    var filter = "";
    
    if (key) {
        key = key.toLowerCase();
        filter = filter + " AND description LIKE '%" + key + "%'";
    }
    if (status) {
        filter = filter + " AND status=" + status;
    }
    var statement = "SELECT COUNT(META(doc).id) AS total "
            + "FROM " + dbConfig.dbName + " doc "
            + "WHERE doc.type='" + documentType + "'" + filter;

    var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
    db.query(query, function(err, result) {
        if (err) {
            log.error("modelArticle.getCount : " + err);
            callback(err, null);
        } else {
            callback(null, result);
        }
    });
};

model.search = function(req, callback) {
    var key = req.body.key;
    var limit = req.body.limit;
    var page = req.body.page;
    var offset = 0;
    var status = req.body.status;
    var filter = "";
    
    if (!limit) {
        limit = 10;
    }
    if (page && page>0) {
        offset = (page - 1) * limit;
    }
    if (status) {
        filter = " AND status=" + status;
    }
    
    var statement = "SELECT META(doc).id, img, description, content, status "
            + "FROM " + dbConfig.dbName + " AS doc " 
            + "WHERE description LIKE '%" + key + "%' AND doc.type='" + documentType 
            + "'" + filter + " ORDER BY doc.id LIMIT " + limit + " OFFSET " + offset;
    var query = queryFactory.fromString(statement);
    db.query(query, function(err, result) {
        if(err) {
            callback(err, null);
        } else {
            callback(null, result);
        }
    });
};

module.exports = model;