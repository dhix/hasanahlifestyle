var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
require('x-date');
var dbConfig = require('../config/dbConfig');
var db = dbConfig.getDb();
var queryFactory = dbConfig.getQueryFactory();
var documentType = "";


function model(docType) {
    documentType = docType;
};

model.save = function(user, callback) {
    var exp = new Date();
    
    exp.setHours(exp.getHours() + 1);
    
    var jsonData = {
        type: documentType,
        email: user.email,
        user_type: user.user_type,
        exp: exp.format('yyyy-mm-dd HH:MM:ss')
    };

    var documentID = new Date().format('yyyymmddHHMMss') + uuid.v4();

    db.upsert(documentID, jsonData, function(error, result) {
        if (error) {
            callback(error, null);
        } else {
            callback(null, {status: "success", message: "Login sukses.", name: user.name, 
                sn: documentID, user_type: user.user_type});
        }
    });
};

model.delete = function(req, callback) {
    var id = req.body.sn;
    
    db.remove(id, function (err, res) {
        callback(null, {status: "success", message: "Proses logout telah dilakukan."});
    });
};

model.deleteAll = function(callback) {
    var now = new Date().format('yyyy-mm-dd HH:MM:ss');
    
    var statement = "DELETE FROM " + dbConfig.dbName + " AS doc "
            + "WHERE type='" + documentType + "' " 
            + "AND STR_TO_MILLIS(doc.exp) < STR_TO_MILLIS('" + now + "')";
    var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
    db.query(query, function(err, result) {
        if (err) {
            callback(err, null);
        } else {
            callback(null, result);
        }
    });
};

model.getById = function(req, callback) {
    var id = req.body.sn;
    
    if (!id) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        db.get(id, function(err, result) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, result.value);
            }
        });
    }
};

module.exports = model;