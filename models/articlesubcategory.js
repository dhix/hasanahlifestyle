var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
var fs = require('fs');
require('x-date');
var appConfig = require('../config/appConfig');
var dbConfig = require('../config/dbConfig');
var db = dbConfig.getDb();
var queryFactory = dbConfig.getQueryFactory();
var documentType = "";


function model(docType) {
    documentType = docType;
};

model.save = function(req, callback) {
    var documentID = req.body.id;
    var id_article_category = req.body.id_article_category;
    var name = req.body.name;
    
    if (!id_article_category || !name) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        var jsonData = {
            type: documentType,
            id_article_category: id_article_category,
            img: req.body.img,
            name: name,
            description: req.body.description
        };

        var add = false;

        if (!documentID) {
            add = true;
            documentID = new Date().format('yyyymmddHHMMss') + uuid.v4();
        }
    
        db.upsert(documentID, jsonData, function(error, result) {
            if(error) {
                callback(error, null);
            } else {
                if (add) {
                    callback(null, {status: "success", message: "Data telah disimpan.", id: documentID});
                } else {
                    callback(null, {status: "success", message: "Data telah diubah.", id: documentID});
                }
            }
        });
    }
};

model.delete = function(req, callback) {
    var id = req.body.id;
    
    if (!id) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        db.get(id, function(err, result) {
            if (err) {
                callback(err, null);
            } else {
                var img = result.value.img;
                if (!img) {
                    callback(null, {status: "success", message: "Data telah dihapus."});
                } else {
                    db.remove(id, function (err, res) {
                        if (err) {
                            callback(err, null);
                        } else {
                            img = img.replace(appConfig.SERVER_ADDRESS, "");
                            fs.exists('./public/images/' + img, function(exists) {
                                if (exists) {
                                    fs.unlink('./public/images/' + img);
                                }
                            });
                            callback(null, {status: "success", message: "Data telah dihapus."});
                        }
                    });
                }
            }
        });
    }
};

model.getById = function(req, callback) {
    var id = req.body.id;
    
    if (!id) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        var statement = "SELECT META(doc).id, doc.id_article_category, " 
                + "articlecategory.name AS name_article_category, "
                + "doc.img, doc.name, doc.description "
                + "FROM " + dbConfig.dbName + " doc "
                + "JOIN " + dbConfig.dbName + " articlecategory "
                + "ON KEYS doc.id_article_category "
                + "WHERE META(doc).id='" + id + "' LIMIT 1";
        var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
        var dbReq = db.query(query);
        dbReq.on('row', function(row) {
            callback(null, row);
        });
        dbReq.on('error', function(err) {
            callback(err, null);
        });
    }
};

model.getAll = function(req, callback) {
    var statement = "SELECT META(doc).id, doc.id_article_category, " 
            + "articlecategory.name AS name_article_category, "
            + "doc.img, doc.name, doc.description "
            + "FROM " + dbConfig.dbName + " doc "
            + "JOIN " + dbConfig.dbName + " articlecategory "
            + "ON KEYS doc.id_article_category "
            + "WHERE doc.type='" + documentType + "' ORDER BY doc.id";

    var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
    db.query(query, function(err, result) {
        if (err) {
            callback(err, null);
        } else {
            callback(null, result);
        }
    });
};

model.getByIdArticleCategory = function(req, callback) {
    var id_article_category = req.body.id_article_category;
    
    if (!id_article_category) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        var statement = "SELECT META(doc).id, doc.id_article_category, " 
                + "articlecategory.name AS name_article_category, "
                + "doc.img, doc.name, doc.description "
                + "FROM " + dbConfig.dbName + " doc "
                + "JOIN " + dbConfig.dbName + " articlecategory "
                + "ON KEYS doc.id_article_category "
                + "WHERE doc.id_article_category='" + id_article_category + "' " 
                + "AND doc.type='" + documentType + "' ORDER BY doc.id";
        var query = queryFactory.fromString(statement).consistency(queryFactory.Consistency.REQUEST_PLUS);
        db.query(query, function(err, result) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, result);
            }
        });
    }
};

model.search = function(req, callback) {
    var key = req.body.key;
    
    if (!key) {
        callback({status: "error", message: "Inputan tidak valid."}, null);
    } else {
        var statement = "SELECT META(doc).id, doc.id_article_category, " 
                + "articlecategory.name AS name_article_category, "
                + "doc.img, doc.name, doc.description "
                + "FROM " + dbConfig.dbName + " doc "
                + "JOIN " + dbConfig.dbName + " articlecategory "
                + "ON KEYS doc.id_article_category "
                + "WHERE doc.description LIKE '%$1%' AND doc.type='" + documentType + "' ORDER BY doc.id";
        var query = queryFactory.fromString(statement);
        db.query(query, [key], function(err, result) {
            if (err) {
                callback(err, null);
            } else {
                callback(null, result);
            }
        });
    }
};

module.exports = model;