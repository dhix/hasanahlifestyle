var couchbase = require("couchbase");
var queryFactory = couchbase.N1qlQuery;

var dbConfig = {
    dbName: "Hasanah"
};

dbConfig.getDb = function() {
    var db = (new couchbase.Cluster("http://localhost:8091")).openBucket(dbConfig.dbName);
    return db;
};

dbConfig.getQueryFactory = function() {
    return queryFactory;
};

module.exports = dbConfig;