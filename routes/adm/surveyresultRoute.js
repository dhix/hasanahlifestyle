var log = require('../../util/logger.js').LOG;
var docType = "";

function srvRoute(clazz) {
    docType = clazz;
};

srvRoute.save = function(req, res) {
    try {
        var model = require("../../models/" + docType); 
        model(docType);
        model.save(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("admSurveyresultRoute.save : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

srvRoute.delete = function(req, res) {
    try {
        var model = require("../../models/" + docType); 
        model(docType);
        model.delete(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("admSurveyresultRoute.save : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

srvRoute.getbyid = function(req, res) {
    try {
        var model = require("../../models/" + docType); 
        model(docType);
        model.getById(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("admSurveyresultRoute.getbyid : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

srvRoute.getall = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.getAll(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("admSurveyresultRoute.getall : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

srvRoute.search = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.search(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("admSurveyresultRoute.search : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

module.exports = srvRoute;