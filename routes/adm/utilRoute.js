var multer = require('multer');
var appConfig = require('../../config/appConfig');
var docType = "";

var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, './public/images/');
    },
    filename: function (req, file, callback) {
        var fileName = file.originalname.split(".");
        var typeFile = fileName[fileName.length - 1];
        var id = req.body.id;
        callback(null, id + "." + typeFile);
    }
});

var fileFilter = function (req, file, callback) {
 
  // The function should call `cb` with a boolean 
  // to indicate if the file should be accepted 
 
  // To reject this file pass `false`, like so: 
//  callback(null, false);
 
  // To accept the file pass `true`, like so: 
//  callback(null, true);
 
  // You can always pass an error if something goes wrong: 
  // callback(new Error('I don\'t have a clue!'));
  
    var model = require("../../models/" + docType);

    var id = req.body.id;
    var fileName = file.originalname.split(".");
    var typeFile = fileName[fileName.length - 1];

    model.getById(id, function(err, result) {
        if (err) {
            callback({status: "error", message: "Data utama tidak ditemukan."}, false);
        } else {
            result.img = appConfig.SERVER_ADDRESS + id + "." + typeFile;
            model.save(id, result, function(err, result) {
                if (err) {
                    callback(err, false);
                } else {
                    callback(null, true);
                }
            });
        }
    });
};

var uploadProscess = multer({
    storage: storage,
    fileFilter: fileFilter
    }).any();

function admRoute(clazz) {
    docType = clazz;
};
    
admRoute.upload = function(req, res) {
    try {
        uploadProscess(req, res, function(err) {
            if (err) {
                res.status(400).send({status: "error", message: "Proses upload file gagal. " + err.message});
            } else {
                res.send({status: "success", message: "Upload file berhasil."});
            }
        });
    } catch (err) {
        res.status(400).send({status: "error", message: err.message});
    }
};

module.exports = admRoute;