var log = require('../../util/logger.js').LOG;
var docType = "";

function srvRoute(clazz) {
    docType = clazz;
};

srvRoute.save = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.save(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("srvUserdeviceRoute.save : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

srvRoute.getbyid = function(req, res) {
    try {
        var model = require("../../models/" + docType); 
        model(docType);
        model.getById(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("srvUserdeviceRoute.getbyid : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

srvRoute.getbyuuid = function(req, res) {
    try {
        var model = require("../../models/" + docType); 
        model(docType);
        model.getByUuid(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("srvUserdeviceRoute.getbyuuid : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

srvRoute.getall = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.getAll(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("srvUserdeviceRoute.getall : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

srvRoute.login = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.login(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("srvUserdeviceRoute.login : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

srvRoute.search = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.search(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("srvUserdeviceRoute.serach : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

module.exports = srvRoute;