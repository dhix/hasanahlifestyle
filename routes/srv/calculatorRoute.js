var log = require('../../util/logger.js').LOG;
var docType = "";

function admRoute(clazz) {
    docType = clazz;
};

admRoute.getbyid = function(req, res) {
    try {
        var model = require("../../models/" + docType); 
        model(docType);
        model.getById(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("srvCalculatorRoute.getbyid : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

admRoute.getall = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.getAll(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("srvCalculatorRoute.getall : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

admRoute.getbytype = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.getByType(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("srvCalculatorRoute.getall : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

admRoute.getbytypeandwork = function(req, res) {
    try {
        var model = require("../../models/" + docType);
        model(docType);
        model.getByTypeAndWork(req, function(err, result) {
            if (err) {
                res.status(400).send({status: "error", message: err.message});
            } else {
                res.send(result);
            }
        });
    } catch (err) {
        log.error("srvCalculatorRoute.getall : " + err);
        res.status(400).send({status: "error", message: err.message});
    }
};

module.exports = admRoute;