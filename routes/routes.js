var log = require('../util/logger.js').LOG;
var uuid = require("uuid");
var multer  = require('multer');

var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, './public/images/');
    },
    filename: function (req, file, callback) {
        var fileName = file.originalname.split(".");
        var typeFile = fileName[fileName.length - 1];
        
        newName = new Date().format('yyyymmddHHMMss') + uuid.v4();
        callback(null, newName + "." + typeFile);
    }
});
var upload = multer({ storage: storage });

var appRouter = function(app) {
    
    app.post('/adm/:clazz/:method', function(req, res) {
        try {
            var clazz = req.params.clazz;
            var method = req.params.method;

            log.info("postAdm : /" + clazz + "/" + method);
            
            var routes = require("./adm/" + clazz + "Route.js");
            
            if (clazz==="user" && method==="login") {
                routes(clazz);
                routes[method](req, res);
            } else if (clazz==="util" && method==="upload") {
                routes(clazz);
                routes[method](req, res);
            } else {
                var check = require("../util/checkToken.js");
                check.token(req, function(result) {
                    if (result===true) {
                        routes(clazz);
                        routes[method](req, res);
                    } else {
                        res.status(498).send({status: "error", message: "Access is not allowed."});
                    }
                });
            }
        } catch (err) {
            log.error("postAdm : " + err);
            res.status(404).send({status: "error", message: "Function not found."});
        }
    });
    
    app.get('/adm/:clazz/:method', function(req, res) {
        try {
            var clazz = req.params.clazz;
            var method = req.params.method;
            
            log.info("getAdm : /" + clazz + "/" + method);
            
            res.status(498).send({status: "error", message: "Access is not allowed."});
        } catch (err) {
            log.error("getAdm : " + err);
            res.status(404).send({status: "error", message: "Function not found."});
        }
    });
    
    
    app.post('/srv/:clazz/:method', function(req, res) {
        try {
            var clazz = req.params.clazz;
            var method = req.params.method;

            log.info("postSrv : /" + clazz + "/" + method);
            
            var routes = require("./srv/" + clazz + "Route.js");
            routes(clazz);
            routes[method](req, res);
        } catch (err) {
            log.error("postSrv : " + err);
            res.status(404).send({status: "error", message: "Function not found."});
        }
    });
    
    app.get('/srv/:clazz/:method', function(req, res) {
        try {
            var clazz = req.params.clazz;
            var method = req.params.method;

            log.info("getSrv : /" + clazz + "/" + method);

            var routes = require("./srv/" + clazz + "Route.js");
            routes(clazz);
            routes[method](req, res);
        } catch (err) {
            log.error("getSrv : " + err);
            res.status(404).send({status: "error", message: "Function not found."});
        }
    });
};

module.exports = appRouter;